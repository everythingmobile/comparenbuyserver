package com.springapp.mvc.Helpers;

import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Model.SiteResponse;
import com.springapp.mvc.Service.ScrapingService;

import java.util.concurrent.Callable;

/**
 * Created by pradeep1 on 8/17/2014.
 */
public class CallableClass implements Callable<SiteResponse> {
    private String responseType;
    private ScrapingService scrapingService;
    private Query query;

    public CallableClass(String responseType, ScrapingService scrapingService, Query query) {
        this.scrapingService = scrapingService;
        this.responseType = responseType;
        this.query = query;
    }

    @Override
    public SiteResponse call() throws Exception {
        return new SiteResponse(responseType, scrapingService.getProductsForPage(query));
    }
}
