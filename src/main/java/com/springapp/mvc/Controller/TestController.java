package com.springapp.mvc.Controller;

import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Service.ScrapingService;
import com.springapp.mvc.Service.ServiceFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vivek
 * Date: 11/24/13
 * Time: 12:10 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TestController {

    @RequestMapping(value = "/flipkart", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        ScrapingService flipkartScrapingService = ServiceFactory.getFlipkartScrapingService();
        if(productName == null || pageNum == null)
        {
            return flipkartScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
        }
        return flipkartScrapingService.getProductsForPage(new Query(productName, pageNum));
    }

    @RequestMapping(value = "/amazon", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test1(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        ScrapingService amazonScrapingService = ServiceFactory.getAmazonScrapingService();
        if(productName == null || pageNum == null)
        {
            return amazonScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
        }
        return amazonScrapingService.getProductsForPage(new Query(productName, pageNum));
    }

    @RequestMapping(value = "/snapdeal", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test2(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        ScrapingService snapdealScrapingService = ServiceFactory.getSnapdealScrapingService();
        if(productName == null || pageNum == null)
        {
            return snapdealScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
        }
        return snapdealScrapingService.getProductsForPage(new Query(productName, pageNum));
    }

    @RequestMapping(value = "/tradus", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test3(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        ScrapingService tradusScrapingService = ServiceFactory.getTradusScrapingService();
        if(productName == null || pageNum == null)
        {
            return tradusScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
        }
        return tradusScrapingService.getProductsForPage(new Query(productName, pageNum));
    }

    @RequestMapping(value = "/infibeam", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test4(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        ScrapingService infibeamScrapingService = ServiceFactory.getInfibeamScrapingService();
        if(productName == null || pageNum == null)
        {
            return infibeamScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
        }
        return infibeamScrapingService.getProductsForPage(new Query(productName, pageNum));
    }

    @RequestMapping(value = "/ebay", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test5(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        ScrapingService ebayScrapingService = ServiceFactory.getEbayScrapingService();
        if(productName == null || pageNum == null)
        {
            return ebayScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
        }
        return ebayScrapingService.getProductsForPage(new Query(productName, pageNum));
    }

    @RequestMapping(value = "/naaptol", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test6(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        ScrapingService naaptolScrapingService = ServiceFactory.getNaaptolScrapingService();
        if(productName == null || pageNum == null)
        {
            return naaptolScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
        }
        return naaptolScrapingService.getProductsForPage(new Query(productName, pageNum));
    }

    @RequestMapping(value = "/homeshop18", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test7(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        ScrapingService homeScrapingService = ServiceFactory.getHomeShop18ScrapingService();
        if(productName == null || pageNum == null)
        {
            return homeScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
        }
        return homeScrapingService.getProductsForPage(new Query(productName, pageNum));
    }
}
