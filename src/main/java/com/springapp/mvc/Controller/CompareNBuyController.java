package com.springapp.mvc.Controller;

import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Model.ResponseAccumulator;
import com.springapp.mvc.Model.SiteResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by vivek on 2/2/14.
 */
@Controller
public class CompareNBuyController
{
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    @ResponseBody
    public List<SiteResponse> products(@RequestParam(value = "q")String productName, @RequestParam(value = "page") String pageNum)
    {
        Query currentQuery = new Query(productName, pageNum);
        return ResponseAccumulator.getResponseAccumulator().getSiteResponses(currentQuery);
    }
}
