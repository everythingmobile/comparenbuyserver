package com.springapp.mvc.Model;

import com.springapp.mvc.Helpers.CallableClass;
import com.springapp.mvc.Service.AutoSuggestion;
import com.springapp.mvc.Service.ServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by vivek on 1/31/14.
 */
public class ResponseAccumulator {

    private static ResponseAccumulator responseAccumulator;
    @Autowired
    private ExecutorService executorService;

    private ResponseAccumulator() {
    }

    public static ResponseAccumulator getResponseAccumulator() {
        if (responseAccumulator == null) {
            responseAccumulator = new ResponseAccumulator();
        }
        return responseAccumulator;
    }


    public List<SiteResponse> getSiteResponses(Query query) {
        List<SiteResponse> responses = new ArrayList<SiteResponse>();
        ExecutorCompletionService executorCompletionService = new ExecutorCompletionService(executorService);
        executorCompletionService.submit(new CallableClass("FlipkartResponse", ServiceFactory.getFlipkartScrapingService(), query));
        executorCompletionService.submit(new CallableClass("AmazonResponse", ServiceFactory.getAmazonScrapingService(), query));
        executorCompletionService.submit(new CallableClass("TradusResponse", ServiceFactory.getTradusScrapingService(), query));
        executorCompletionService.submit(new CallableClass("SnapdealResponse", ServiceFactory.getSnapdealScrapingService(), query));
        executorCompletionService.submit(new CallableClass("InfibeamResponse", ServiceFactory.getInfibeamScrapingService(), query));
        executorCompletionService.submit(new CallableClass("NaaptolResponse", ServiceFactory.getNaaptolScrapingService(), query));
        executorCompletionService.submit(new CallableClass("EbayResponse", ServiceFactory.getEbayScrapingService(), query));
        executorCompletionService.submit(new CallableClass("HomeShop18Response", ServiceFactory.getHomeShop18ScrapingService(), query));

        Map<String, SiteResponse> map = new HashMap<String, SiteResponse>();
        for (int i = 0; i < 8; i++) {
            try {
                Future<SiteResponse> future = executorCompletionService.take();
                SiteResponse siteResponse = future.get();
                map.put(siteResponse.getSiteName(), siteResponse);

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException ex) {
                ex.printStackTrace();
            }
        }

        responses.add(map.get("FlipkartResponse"));
        responses.add(map.get("AmazonResponse"));
        responses.add(map.get("TradusResponse"));
        responses.add(map.get("SnapdealResponse"));
        responses.add(map.get("InfibeamResponse"));
        responses.add(map.get("NaaptolResponse"));
        responses.add(map.get("EbayResponse"));
        responses.add(map.get("HomeShop18Response"));

        responses.add(new SiteResponse("AutoSuggest", new AutoSuggestion(responses).getSuggestedList()));
        return responses;
    }
}
