package com.springapp.mvc.Model;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by vivek on 4/20/14.
 */
public class Constants
{
    private static Properties prop = null;

    private Constants() {

    }

    public static Properties getInstance() {

        if (prop == null) {
            ClassLoader loader = Constants.class.getClassLoader();
            if (loader == null)
                loader = ClassLoader.getSystemClassLoader();

            String propFile = "development.properties";


            try {
                java.net.URL url = loader.getResource(propFile);
                prop = new Properties();
                prop.load((url.openStream()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }


        return prop;
    }
}
