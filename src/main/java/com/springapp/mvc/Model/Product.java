package com.springapp.mvc.Model;

/**
 * Created with IntelliJ IDEA.
 * User: vivek
 * Date: 11/23/13
 * Time: 6:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class Product
{
	private String name;
	private String price;
	private String imageUrl;
	private String productLink;
    private String siteName;

	public Product(String name, String price, String imageUrl, String productLink)
	{
		this.name = name;
		this.price = price;
		this.imageUrl = imageUrl;
		this.productLink = productLink;
	}

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public void setName(String name)
	{
		this.name = name;
	}

	public void setPrice(String price)
	{
		this.price = price;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	public void setProductLink(String productLink)
	{
		this.productLink = productLink;
	}

	public String getName()
	{

		return name;
	}

	public String getPrice()
	{
		return price;
	}

	public String getImageUrl()
	{
		return imageUrl;
	}

	public String getProductLink()
	{
		return productLink;
	}

	public String toString()
	{
		return getName() + "|" + getPrice() + "|" + getProductLink() + "|" + getImageUrl();
	}

    public int getIntPrice()
    {
        String priceString = getPrice();
        if(priceString.contains("-"))
            priceString = priceString.split("-")[0];
        priceString = priceString.replaceAll("\\s+", "");
        priceString = priceString.replaceAll("\\u00A0", "");
        priceString = priceString.replace(",", "");
        priceString = priceString.replace("Rs.", "");
        priceString = priceString.replace("Rs", "");

        try{
            return Double.valueOf(priceString).intValue();
        }
        catch (Exception e) {
            return 10000000;
        }
    }
}
