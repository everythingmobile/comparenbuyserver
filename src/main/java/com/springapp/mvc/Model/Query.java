package com.springapp.mvc.Model;

/**
 * Created by vivek on 1/31/14.
 */
public class Query {
    private String productName;
    private String pageNumber;

    public Query(String productName, String pageNumber) {
        this.productName = productName.trim().replaceAll(" +", "+");
        try {
            this.pageNumber = Integer.toString(Integer.parseInt(pageNumber));
        } catch (Exception e) {
            this.pageNumber = "1";
        }
    }

    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass())
            result = false;
        else {
            Query query = (Query) object;
            if (this.productName.equals(query.productName) && this.pageNumber.equals(query.pageNumber))
                result = true;
        }
        return result;

    }

    @Override
    public int hashCode() {
        return this.productName.hashCode() + this.pageNumber.hashCode();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }
}
