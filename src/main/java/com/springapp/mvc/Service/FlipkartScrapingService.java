package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Constants;
import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: vivek
 * Date: 11/23/13
 * Time: 11:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class FlipkartScrapingService extends ScrapingService {
    private static Properties properties = Constants.getInstance();

    private String getProductTitleFromElement(Element element) {
        return element.select(properties.getProperty("flipkart.product.displayBlock")).first().attr("title");
    }

    private String getProductImageUrlFromElement(Element element) {
        String imgUrl = element.select("img").first().attr(properties.getProperty("flipkart.product.imgAttrName"));
        if (imgUrl == "")
            imgUrl = element.select("img").first().attr(properties.getProperty("flipkart.product.imgAttrNameIfNot"));
        return imgUrl;
    }

    private String getProductLinkFromElement(Element element) {
        return "http://www.flipkart.com" + element.select(properties.getProperty("flipkart.product.displayBlock")).first().attr("href") + properties.getProperty("flipkart.affil");
    }

    private String getProductPriceFromElement(Element element) {
        return element.select(properties.getProperty("flipkart.product.prodPriceAttrName")).text();
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {
        String url = "http://www.flipkart.com/search?q=";
        url += query.getProductName();
        url += "&start=";
        int page = Integer.parseInt(query.getPageNumber());
        page = 1 + (page - 1) * 20;
        url += page;
        List<Product> productList = new ArrayList<Product>();


        try {
            Document document = Jsoup.connect(url).timeout(10000).get();
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("flipkart.fetchNumber"));
            Elements elements = document.select(properties.getProperty("flipkart.product.holderDiv"));
            int count = 0;
            for (Element element : elements) {
                if (count == maximumProductsToFetch) break;
                count++;

                Product product = new Product(
                        getProductTitleFromElement(element), getProductPriceFromElement(element), getProductImageUrlFromElement(element), getProductLinkFromElement(element)
                );

                productList.add(product);

                //logger.log(s+" at price "+price+" image url "+img+" link to product "+prodLink);
            }
            if (elements == null || elements.isEmpty() == true) {
                elements = document.select(properties.getProperty("flipkart.product.productsDiv"));
                for (Element ele : elements) {
                    if (count == maximumProductsToFetch) break;
                    count++;
                    Element imageDiv = ele.select(properties.getProperty("flipkart.product.imageDiv")).first();
                    String image = imageDiv.attr(properties.getProperty("flipkart.product.imgAttrName"));
                    if (image.equals("")) {
                        image = imageDiv.attr(properties.getProperty("flipkart.product.imgAttrNameIfNot"));
                    }
                    Element titleDiv = ele.select(properties.getProperty("flipkart.product.titleDiv")).first();
                    String title = titleDiv.text();
                    Element priceDiv = ele.select(properties.getProperty("flipkart.product.priceDiv")).first();
                    String price = priceDiv.text();
                    String productLink = "http://www.flipkart.com" + titleDiv.attr("href");
                    Product product = new Product(title, price, image, productLink);
                    if (product != null) productList.add(product);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }
}
