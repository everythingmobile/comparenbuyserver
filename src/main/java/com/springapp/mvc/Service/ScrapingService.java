package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: vivek
 * Date: 11/23/13
 * Time: 6:12 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class ScrapingService {
    Map<Query, List<Product>> productsCache = new HashMap<Query, List<Product>>();
    Map<Query, Long> lastTime = new HashMap<Query, Long>();
    private Long maxTTL = 86400000L;

    public abstract List<Product> runSiteRoutine(Query query);

    public List<Product> getProductsForPage(Query query) {

        if (productsCache.containsKey(query) && productsCache.get(query) != null && productsCache.get(query).size() > 0) {
            if((new Date()).getTime() - lastTime.get(query) < maxTTL)
                return productsCache.get(query);
        }

        List<Product> products = runSiteRoutine(query);
        if (products != null && products.size() > 0) {
            lastTime.put(query, (new Date()).getTime());
            productsCache.put(query, products);
        }


        return sanitize(products);
    }

    private List<Product> sanitize(List<Product> products)
    {
        List<Product> ret = new ArrayList<Product>(0);
        for(Product product: products)
        {
            if(product.getPrice().equals("") || product.getPrice().equals("Rs.") || product.getPrice().equals("Rs"))
                continue;
            if(product.getName().equals(""))
                continue;
            ret.add(product);
        }
        return ret;
    }

}
