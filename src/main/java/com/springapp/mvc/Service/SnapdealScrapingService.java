package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Constants;
import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: pradeep1
 * Date: 11/23/13
 * Time: 11:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class SnapdealScrapingService extends ScrapingService {
    private static Properties properties = Constants.getInstance();

    public List<Product> GetProductsByCategory(Query query, String category) {
        String productName = URLEncoder.encode(query.getProductName());
        List<Product> productList = new ArrayList<Product>();
        int maxResults = 20;
        int page = (Integer.parseInt(query.getPageNumber()) - 1) * maxResults;
        String u = "http://www.snapdeal.com/acors/json/product/get/search/" + category + "/" + page + "/" + maxResults + "?sort=rlvncy&keyword=" + productName;
        URL link = null;
        try {
            link = new URL(u);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        ObjectMapper mapper = new ObjectMapper();
        Map<String, List<LinkedHashMap<String, Object>>> jsonMap = null;
        try {
            jsonMap = mapper.readValue(link, Map.class);
            List<LinkedHashMap<String, Object>> products = jsonMap.get("productDtos");
            if (products == null) return productList;
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("snapdeal.fetchNumber"));
            int count = 0;
            for (LinkedHashMap<String, Object> item : products) {
                if (count == maximumProductsToFetch) break;
                count++;
                String name = item.get("name") != null ? item.get("name").toString() : "";
                String price = item.get("displayPrice") != null ? item.get("displayPrice").toString() : "";
                String image = "http://n.sdlcdn.com/" + item.get("image");
                String productLink = "http://www.snapdeal.com/" + item.get("pageUrl");
                productLink += getAffiliateString(productLink);
                Product product = new Product(name, price, image, productLink);
                productList.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return productList;

    }

    private String getAffiliateString(String productLink) {
        return productLink.contains("?") ? (String) properties.get("snapdeal.affil1") : (String) properties.get("snapdeal.affil2");
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {
        String url = "http://www.snapdeal.com/search?keyword=";
        String productName = URLEncoder.encode(query.getProductName());
        url += productName;
        url += "&santizedKeyword=&catId=&categoryId=0&suggested=false&vertical=&noOfResults=20&clickSrc=go_header&lastKeyword=&prodCatId=&changeBackToAll=false&foundInAll=false&categoryIdSearched=&cityPageUrl=&url=&utmContent=&catalogID=&dealDetail=";
        List<Product> productList = new ArrayList<Product>();

        try {
            Document document = Jsoup.connect(url).get();
            Element element = document.select(".product_list_view_cont.overhid").first();
            if (element != null) {
                //element = element.select("a").first();
                //if (element == null) return productList;
                String category = element.attr("categoryid");
                return GetProductsByCategory(query, category);

            } else {
                element = document.getElementById("catId");
                if (element == null || element.attr("value").toString().equals("")) {
                    element = document.getElementById("categoryId");
                }

                if (element == null || element.attr("value").toString().equals("")) {
                    element = document.getElementById("selectedTabId");

                }
                if (element == null) return productList;
                String category = element.attr("value");
                return GetProductsByCategory(query, category);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return productList;
    }
}
