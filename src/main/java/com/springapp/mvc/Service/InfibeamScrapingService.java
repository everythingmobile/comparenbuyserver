package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Constants;
import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Service.FlipkartScrapingService;
import com.springapp.mvc.Service.ScrapingService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class InfibeamScrapingService extends ScrapingService {
    private static Properties properties = Constants.getInstance();

    private String getProductTitleFromElement(Element element) {
        String title = element.select(properties.getProperty("infibeam.product.image")).attr(properties.getProperty("infibeam.product.title"));
        return title;
    }

    private String getProductPriceFromElement(Element element) {
        String price = "";
        Element priceElement = element.select(properties.getProperty("infibeam.product.priceDiv")).first();
        if (priceElement == null)
            System.out.print(element);
        else {
            if (priceElement.select(properties.getProperty("infibeam.product.priceSpan")).isEmpty() == false) {
                price = priceElement.select(properties.getProperty("infibeam.product.priceSpan")).first().text();
            } else {
                price = priceElement.select(properties.getProperty("infibeam.product.priceAlt")).first().text();
            }
        }

        return price;
    }

    private String getProductImageUrlFromElement(Element element) {
        String img = element.getElementsByTag(properties.getProperty("infibeam.product.image")).first().attr(properties.getProperty("infibeam.product.imageAttr"));
        return img;
    }

    private String getProductLinkFromElement(Element element) {
        String link = element.select(properties.getProperty("infibeam.product.link")).first().attr(properties.getProperty("infibeam.product.linkAttr"));
        return addAffil("http://www.infibeam.com", link);
    }

    private String addAffil(String domain,String link) {
        String aff = getAffilFromLink(link);
        if(link.indexOf('#') != -1)
        {
            StringBuffer stringBuffer = new StringBuffer(domain);
            String[] parts = link.split("#");
            if(parts.length < 2)
                return domain + link;
            stringBuffer.append(parts[0]);
            stringBuffer.append(aff);
            stringBuffer.append('#');
            stringBuffer.append(parts[1]);
            return stringBuffer.toString();
        }
        return domain + link + getAffilFromLink(link);
    }

    private String getAffilFromLink(String link) {
        if(link.contains("?"))
            return properties.getProperty("infibeam.affil1");
        return properties.getProperty("infibeam.affil2");
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {
        String url = "http://www.infibeam.com/search?q=";
        // url+=productName;
        url += URLEncoder.encode(query.getProductName());
        url += "&page=" + query.getPageNumber();
        List<Product> productList = new ArrayList<Product>();

        try {
            Document document = Jsoup.connect(url).userAgent("Chrome").get();
            Elements elements = document.select(properties.getProperty("infibeam.product.holderUl"));
            Elements elements1 = new Elements();
            for (Element e : elements) {
                elements1.addAll(e.select(properties.getProperty("infibeam.product.holderLi")));
            }
            int count = 0;
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("infibeam.fetchNumber"));
            for (Element element : elements1) {
                if (count == maximumProductsToFetch) break;
                count++;

                Product product = new Product(
                        getProductTitleFromElement(element), getProductPriceFromElement(element), getProductImageUrlFromElement(element), getProductLinkFromElement(element)
                );
                productList.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productList;
    }
}
