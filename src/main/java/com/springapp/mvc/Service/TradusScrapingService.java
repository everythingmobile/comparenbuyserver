package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Constants;
import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Service.FlipkartScrapingService;
import com.springapp.mvc.Service.ScrapingService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: pradeep1
 * Date: 11/24/13
 * Time: 8:48 PM
 * To change this template use File | Settings | File Templates.
 */

public class TradusScrapingService extends ScrapingService {
    private static Properties properties = Constants.getInstance();

    private String getProductTitleFromElement(Element element) {
        String title = element.select(properties.getProperty("tradus.product.titleDiv")).first().text();
        return title;
    }

    private String getProductPriceFromElement(Element element) {
        String price;
        price = element.select(properties.getProperty("tradus.product.priceHolder")).first().text();
        return price;
    }

    private String getProductImageUrlFromElement(Element element) {
        String img;
        Element imgEl = element.select(properties.getProperty("tradus.product.imageHolder")).first();
        img = imgEl.getElementsByAttribute(properties.getProperty("tradus.product.imageAttr")).first().attr(properties.getProperty("tradus.product.imageAttr"));
        return img;
    }

    private String getProductLinkFromElement(Element element) {
        String link = element.select(properties.getProperty("tradus.product.linkHolder")).first().attr("href");
        return "http://www.tradus.com" + link;
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {
        String url = "http://www.tradus.com/search?query=";
        url += query.getProductName();
        url += "&page=" + query.getPageNumber();
        List<Product> productList = new ArrayList<Product>();
        try {
            Document document = Jsoup.connect(url).userAgent("Mozilla").cookie("auth", "token").timeout(3000).get();
            Elements elements = document.select(properties.getProperty("tradus.product.holder"));
            int count = 0;
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("tradus.fetchNumber"));
            for (Element element : elements) {
                if (count == maximumProductsToFetch) break;
                count++;

                Product product = new Product(
                        getProductTitleFromElement(element), getProductPriceFromElement(element), getProductImageUrlFromElement(element), getProductLinkFromElement(element)
                );

                productList.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productList;
    }
}
