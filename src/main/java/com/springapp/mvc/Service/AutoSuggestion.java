package com.springapp.mvc.Service;

import com.springapp.mvc.Helpers.KMeans;
import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.SiteResponse;

import java.util.*;

/**
 * Created by vivek on 4/12/14.
 */
public class AutoSuggestion
{
    private List<SiteResponse> siteResponses;
    private int numberResponses = 3;
    private int numberProducts = 5;

    public AutoSuggestion(List<SiteResponse> siteResponses)
    {
        this.siteResponses = siteResponses;
    }

    public List<Product> getSuggestedList()
    {
        List<Integer> accuratePrices = new ArrayList<Integer>();
        List<Product> ret = new ArrayList<Product>();
        for(int i=0; i < numberResponses; i++)
        {
            SiteResponse currentResponse = siteResponses.get(i);
            List<Product> currentProducts = currentResponse.getProducts();
            for(int j=0; j < numberProducts; j++)
            {
                if(currentProducts == null || currentProducts.size() <= j || currentProducts.get(j) == null)
                    continue;
                accuratePrices.add(sanitizePrice(currentProducts.get(j).getPrice()));
            }
        }

        List<List<Integer>> clusters = new KMeans().clusterize(accuratePrices, 3);
        int min1 = minimum(clusters.get(0)),
            min2 = minimum(clusters.get(1)),
            min3 = minimum(clusters.get(2));
        int minPrice = getRightPrice(min1, min2, min3);
        //System.out.println("-----" + minPrice);

        for(SiteResponse response : siteResponses)
        {
            for(Product product : response.getProducts())
            {
                product.setSiteName(response.getSiteName());
                int price = sanitizePrice(product.getPrice());
                if(minPrice <= price)
                {
                    ret.add(product);
                }
            }
        }

        sortListByPriceAscending(ret);
        return ret;
    }

    private int getRightPrice(int min1, int min2, int min3) {
        int actMin1, actMin2, actMin3;
        List<Integer> actualList = new ArrayList<Integer>(3);
        actualList.add(min1);
        actualList.add(min2);
        actualList.add(min3);
        Collections.sort(actualList);
        actMin1 = actualList.get(0);
        actMin2 = actualList.get(1);
        actMin3 = actualList.get(2);
        if(actMin2 != Integer.MAX_VALUE)
            return actMin2;
        if(actMin3 != Integer.MAX_VALUE)
            return actMin3;
        return actMin1;
    }

    private void sortListByPriceAscending(List<Product> productList)
    {
        Collections.sort(productList, new Comparator<Product>() {
            @Override
            public int compare(Product lhs, Product rhs) {
                return lhs.getIntPrice() - rhs.getIntPrice();
            }
        });
    }

    private Integer sanitizePrice(String priceString)
    {
        priceString = priceString.replaceAll("\\s+", "");
        priceString = priceString.replaceAll("\\u00A0", "");
        priceString = priceString.replace(",", "");
        priceString = priceString.replace("Rs.", "");
        priceString = priceString.replace("Rs", "");

        try{
            return Double.valueOf(priceString).intValue();
        }
        catch (Exception e) {
            return 0;
        }
    }

    private Integer percentage(int a, int b)
    {
        return ((b-a)/a)*100;
    }

    private Integer average(List<Integer> prices)
    {
        if(prices.size() == 0)
            return 0;
        int temp = 0;
        for(Integer price : prices)
        {
            temp += price;
        }
        return temp/prices.size();
    }

    private Integer minimum(List<Integer> prices)
    {
        if (prices.size() == 0)
            return Integer.MAX_VALUE;
        int min = Integer.MAX_VALUE;
        for(Integer price : prices)
        {
            //System.out.println(price+"ssssss");
            if(price < min)
                min = price;
        }
        return min;
    }

    private Integer standardDeviation(List<Integer> prices, Integer average)
    {
        if(prices.size() == 0)
            return 0;
        Long temp = 0L, variance;
        for(Integer price : prices)
        {
            Double ex = Math.pow(price - average, 2);
            temp += ex.intValue();
        }
        variance = temp/prices.size();
        return ((Double)Math.pow(variance, 0.5)).intValue();
    }
}
