package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Constants;
import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Service.FlipkartScrapingService;
import com.springapp.mvc.Service.ScrapingService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class EbayScrapingService extends ScrapingService {
    private static Properties properties = Constants.getInstance();

    private String getProductTitleFromElement(Element element) {
        Element titleElement = element.select(properties.getProperty("ebay.product.titleHolder")).first();
        String title = titleElement.text();
        return title;
    }

    private String getProductPriceFromElement(Element element) {
        String price = "";
        price = element.select(properties.getProperty("ebay.product.priceHolder")).text();
        return price;
    }

    private String getProductImageUrlFromElement(Element element) {
        String img = "";
        Element imgEl = element.select(properties.getProperty(("ebay.product.imageHolder"))).first();
        img = imgEl.select(properties.getProperty("ebay.product.img")).attr(properties.getProperty("ebay.product.imgAttr"));
        return img;
    }

    private String getProductLinkFromElement(Element element) {
        String link = "";
        Element titleElement = element.select(properties.getProperty("ebay.product.titleHolder")).first();
        link = titleElement.select(properties.getProperty("ebay.product.link")).attr(properties.getProperty("ebay.product.linkAttr"));
        return link;
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {
        String url = "http://www.ebay.in/sch/i.html?_nkw=";
        url += query.getProductName();
        url += "&LH_BIN=1";
        url += "&_pgn=" + query.getPageNumber();
        List<Product> productList = new ArrayList<Product>();

        try {
            Document document = Jsoup.connect(url).userAgent("Mozilla").get();
            Elements elements = document.select(properties.getProperty("ebay.product.holder"));
            int count = 0;
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("ebay.fetchNumber"));
            for (Element element : elements) {
                if (count == maximumProductsToFetch) break;
                count++;

                Product product = new Product(
                        getProductTitleFromElement(element), getProductPriceFromElement(element), getProductImageUrlFromElement(element), getProductLinkFromElement(element)
                );
                productList.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productList;
    }
}
