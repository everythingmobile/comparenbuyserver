package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Constants;
import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Service.FlipkartScrapingService;
import com.springapp.mvc.Service.ScrapingService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: pradeep1
 * Date: 11/24/13
 * Time: 8:48 PM
 * To change this template use File | Settings | File Templates.
 */

public class AmazonScrapingService extends ScrapingService {
    private static Properties properties = Constants.getInstance();

    private String getProductTitleFromElement(Element element) {
        String title = element.select(properties.getProperty("amazon.product.titleSpan")).first().text();
        return title;
    }

    private String getProductPriceFromElement(Element element) {
        String price = "";
        if (element.select(properties.getProperty("amazon.product.priceSpan")).first() != null) {
            price = element.select(properties.getProperty("amazon.product.priceSpan")).first().text();

        } else if (element.select(properties.getProperty("amazon.product.altPriceSpan")).first() != null)
            price = element.select(properties.getProperty("amazon.product.altPriceSpan")).first().text();

        return price;
    }

    private String getProductImageUrlFromElement(Element element) {
        String img = element.select("img").first().attr(properties.getProperty("amazon.product.imageAttrName"));
        return img;
    }

    private String getProductLinkFromElement(Element element) {
        String link = element.select("a").first().attr("href");
        return link+properties.getProperty("amazon.affil");
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {

        String url = "http://www.amazon.in/s?field-keywords=";
        url += query.getProductName();
        url += "&page=" + query.getPageNumber();
        List<Product> productList = new ArrayList<Product>();

        try {
            Document document = Jsoup.connect(url).userAgent("Mozilla/5.0").get();
            Elements elements = document.select(properties.getProperty("amazon.product.holderDiv"));

            int count = 0;
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("amazon.fetchNumber"));
            for (Element element : elements) {
                if (count == maximumProductsToFetch) break;
                count++;

                Product product = new Product(
                        getProductTitleFromElement(element), getProductPriceFromElement(element), getProductImageUrlFromElement(element), getProductLinkFromElement(element)
                );
                productList.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productList;
    }
}
